#!/usr/bin/env python
# coding: utf-8

# In[113]:


from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.layers import Embedding
from keras.layers import Conv1D, GlobalAveragePooling1D, MaxPooling1D

import pandas as pd
import numpy as np


# In[114]:


data = pd.read_csv("/Users/yangseongjun/Desktop/result1_1.csv")

data = data.replace(0, -1)


# In[115]:


x_train_csi = data.loc[:,['CSI_Accuracy','CSI_Detection']]

x_train_audio = data.loc[:,['Audio_Accuracy','Audio_Detection']]

y_train = data.loc[:,['Deepfusion_Real']]


# In[116]:


x_train_csi = np.reshape(x_train_csi.values, (1, x_train_csi.values.shape[0], x_train_csi.values.shape[1]))

x_train_audio = np.reshape(x_train_audio.values, (1, x_train_audio.values.shape[0], x_train_audio.values.shape[1]))

y_train = np.reshape(y_train.values, (1, y_train.values.shape[0]))


# In[117]:


data_csi_length = len(x_train_csi[0])

model_csi = Sequential()
model_csi.add(Conv1D(64, 2, activation='relu', input_shape=(data_csi_length, 2))) # 데이터 길이
model_csi.add(Conv1D(64, 2, activation='relu'))
model_csi.add(MaxPooling1D(2))
model_csi.add(Conv1D(128, 2, activation='relu'))
model_csi.add(Conv1D(128, 2, activation='relu'))
model_csi.add(GlobalAveragePooling1D())
model_csi.add(Dropout(0.5))
model_csi.add(Dense(data_csi_length, activation='sigmoid')) # 데이터 길이

model_csi.compile(loss='binary_crossentropy',
              optimizer='rmsprop',
              metrics=['accuracy'])


# In[118]:


model_csi.fit(x_train_csi, y_train, batch_size=16, epochs=10)


# In[119]:


output_csi = model_csi.predict(x_train_csi[:1])
print(output_csi)


# In[120]:


data_audio_length = len(x_train_audio[0])

model_audio = Sequential()
model_audio.add(Conv1D(64, 2, activation='relu', input_shape=(data_audio_length, 2))) # 데이터 길이
model_audio.add(Conv1D(64, 2, activation='relu'))
model_audio.add(MaxPooling1D(2))
model_audio.add(Conv1D(128, 2, activation='relu'))
model_audio.add(Conv1D(128, 2, activation='relu'))
model_audio.add(GlobalAveragePooling1D())
model_audio.add(Dropout(0.5))
model_audio.add(Dense(data_audio_length, activation='sigmoid')) # 데이터 길이

model_audio.compile(loss='binary_crossentropy',
              optimizer='rmsprop',
              metrics=['accuracy'])


# In[121]:


model_audio.fit(x_train_audio, y_train, batch_size=16, epochs=10)


# In[122]:


output_audio = model_csi.predict(x_train_audio[:1])
print(output_audio)


# In[123]:


csi_weight = 0.0

for element in output_csi :
    csi_weight = csi_weight + element[0]

csi_weight = csi_weight / len(output_csi)

audio_weight = 0.0

for element in output_audio :
    audio_weight = audio_weight + element[0]
    
audio_weight = audio_weight / len(output_audio)


# In[124]:


print(csi_weight, audio_weight)


# In[127]:


f = open('weight.txt', mode='wt', encoding='utf-8')

f.write(str(csi_weight) + " ")
f.write(str(audio_weight))    

f.close()


# In[ ]:




