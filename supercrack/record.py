import pyaudio
import wave
import argparse
import logging.config
import threading
import time
import re
import os
import numpy as np
from scipy.io import wavfile

from audio.captor import Captor
from audio.processor import WavProcessor, format_predictions

import tensorflow as tf
import numpy as np
import matlab.engine

import pandas as pd

parser = argparse.ArgumentParser(description='Capture and process audio')
parser.add_argument('--min_time', type=float, default=3.5, metavar='SECONDS',
                    help='Minimum capture time')
parser.add_argument('--max_time', type=float, default=5, metavar='SECONDS',
                    help='Maximum capture time')

f = open("weight.txt", 'r')
line = f.readline().split()
f.close()

csi_weight = int(line[0])
audio_weight = int(line[1])

data = {'CSI_Accuracy':[0],
        'CSI_Detection':[0],
        'Audio_Accuracy':[0],
        'Audio_Detection':[0],
        'Deepfusion_Detection':[0],
        'Deepfusion_Real':[0]
       }

datas = pd.DataFrame(data)

class Capture(object):
    chunk = 1024
    sample_format = pyaudio.paInt16
    channels = 1
    fs = 44100
    seconds = 5
    sleep=0.05
    _process_buf =None
    _ask_data =None
    _save_path=None


    def __init__(start,min_time,max_time, path=None):

        start._save_path = path
        start._ask_data = threading.Event()
        start._captor = Captor(min_time,max_time, start._ask_data, start._process)

    def _process(start, data):
        start._process_buf = np.frombuffer(data, dtype=np.int16)


    def start(start):
        start._captor.start()
        start._process_loop()

    def _process_loop(start):
        detected_csi = ""
        detected_audio = ""

        eng = matlab.engine.start_matlab()
        saver = tf.train.import_meta_graph('../model/model.ckpt.meta')
        graph = tf.get_default_graph()
        x = graph.get_tensor_by_name("Placeholder:0")
        pred = graph.get_tensor_by_name("add:0")
        b = np.arange(0, 15000)
        act = ["Bed", "Fall", "Walk", "Pickup", "Run", "Sitdown", "Run", "Sitdown", "Standup"]

        with WavProcessor() as proc:
            start._ask_data.set()

            while True :
                k = 1
                t = 0
                csi_trace = eng.read_bf_file('/home/kjlee/linux-80211n-csitool-supplementary/netlink/test.dat')

                if len(csi_trace) < 500:
                    continue
                ARR_FINAL = np.empty([0, 90], float)
                xx = np.empty([1, 500, 90], float)
                xx1 = np.empty([0], float)
                yy1 = np.empty([0], float)
                zz1 = np.empty([0], float)
                try:
                    while (k <= 500):
                        csi_entry = csi_trace[t]
                        try:
                            csi = eng.get_scaled_csi(csi_entry)
                            A = eng.abs(csi)
                            ARR_OUT = np.empty([0], float)

                            ARR_OUT = np.concatenate((ARR_OUT, A[0][0]), axis=0)
                            ARR_OUT = np.concatenate((ARR_OUT, A[0][1]), axis=0)
                            ARR_OUT = np.concatenate((ARR_OUT, A[0][2]), axis=0)

                            xx1 = np.concatenate((xx1, A[0][0]), axis = 0)
                            yy1 = np.concatenate((yy1, A[0][1]), axis = 0)
                            zz1 = np.concatenate((zz1, A[0][2]), axis = 0)
                            ARR_FINAL = np.vstack((ARR_FINAL, ARR_OUT))
                            k = k + 1
                            t = t + 1
                        except matlab.engine.MatlabExecutionError:
                            print('MatlabExecutionError occured!!!')
                            break
                    xx[0] = ARR_FINAL
                except ValueError:
                    print('ValueError occured!!!')
                    continue

                with tf.Session(config=tf.ConfigProto(log_device_placement=True)) as sess:
                    saver.restore(sess, '../model/model.ckpt')
                    n = pred.eval(feed_dict={x: xx}) ##############################
                    n2 = tf.argmax(n, 1)
                    result = n2.eval()
                    print(act[int(result)]) ####################
                    detected_csi = act[int(result)]

                    sess.close()

                if start._process_buf is None:
                    time.sleep(start.sleep)
                    continue
                start._ask_data.clear()
                print('Recording :')
                predictions = proc.get_predictions(
                    start.fs,start._process_buf) #######################
                rate_max=str("".join(filter(str.isdigit, (format_predictions(predictions)))))
                sound_max=format_predictions(predictions)
                if(rate_max[0:3]=="100") :
                    searchObj = re.search( r'(.*): 1.00(.*?) .*', format_predictions(predictions), re.M|re.I)
                    print ("Type Sound : ", searchObj.group(1),rate_max[0:3]) ###############
                    detected_audio = searchObj.group(1),rate_max[0:3]
                else :
                    print("Not detection --"rate_max[0:3])
                    detected_audio = searchObj.group(1),rate_max[0:3]

                if (detected_csi == "Bed") | (detected_csi == "Fall") :
                    detected_csi = 1 # Abnormal
                else :
                    detected_csi = 0 # Normal

                if (detected_audio == "Cough") | (detected_audio == "Grown") | (detected_audio == "Wheeze") | (detected_audio == "Scream") | (detected_audio == "Sniff") | (detected_audio == "Crying") :
                    detected_audio = 1 # Abnormal
                else :
                    detected_audio = 0 # Normal

                # weight

                if n * csi_weight > predictions * audio_weight : # 가중치 비교
                    rows = [n, detected_csi, predictions, detected_audio , detected_csi, 0]
                    datas.loc[len(datas)] = rows
                    if detected_csi == 1 :
                        print("Abnormal")
                    else :
                        print("Normal")
                else : # 가중치 비교
                    rows = [n, detected_csi, predictions, detected_audio , detected_audio, 0]
                    datas.loc[len(datas)] = rows
                    if detected_audio == 1 :
                        print("Abnormal")
                    else :
                        print("Normal")


                start._process_buf= None
                start._ask_data.set()


if __name__ == '__main__':
    args = parser.parse_args()
    c = Capture(**vars(args))
    c.start()
    datas.to_csv('/Users/yangseongjun/Desktop/result2.csv')

#############################################################################
